'use strict';

var gulp = require('gulp'),
		sass = require('gulp-sass'),
		concat = require('gulp-concat'),
		uglify = require('gulp-uglify'),
		newer = require('gulp-newer'),
    browserSync = require('browser-sync').create(),
		imagemin = require('gulp-imagemin'),
    gulpCopy = require('gulp-copy');

var imgSrc = './src/images/**/*',
		imgDest = './dist/images',
		sassSrc = './src/scss/**/*.scss',
		sassDest = './dist/css',
    templatejsSrc = './src/js/templatejs/**/*.js',
    jsPagesSrc = './src/js/pages/*.js',
    jsPagesDest = './dist/js/pages',
    htmlFiles = ['src/*.html'],
    htmlDest = './dist';

//sass
gulp.task('sass', function () {
  return gulp.src(sassSrc)
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(gulp.dest(sassDest))
    .pipe(browserSync.stream());
});
 
gulp.task('sass:watch', function () {
  gulp.watch(sassSrc, ['sass']);
});

//images
gulp.task('images', function() {
  return gulp.src(imgSrc)
    .pipe(newer(imgDest))
    .pipe(imagemin())
    .pipe(gulp.dest(imgDest))
    .pipe(browserSync.stream());
});

gulp.task('images:watch', function() {
  gulp.watch(imgSrc, ['images']);
});

//html
gulp.task('html', function () {
    gulp.src('./src/*.html')
    .pipe(gulp.dest('./dist/'))
    .pipe(browserSync.stream());
});
gulp.task('html:watch', function() {
  gulp.watch(htmlFiles, ['html']);
});

//js
gulp.task('templatejs', function () {
    gulp.src(templatejsSrc)
    .pipe(gulp.dest('./dist/js'));
});

//js pages
gulp.task('jsPages', function() {
  return gulp.src([jsPagesSrc])
    .pipe(uglify())
    .pipe(gulp.dest(jsPagesDest))
    .pipe(browserSync.stream());
});

gulp.task('jsPages:watch', function() {
  gulp.watch(jsPagesSrc, ['jsPages']);
});

// Static server
gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "./dist"
        }
    });
});

//watch
gulp.task('watch', ['browser-sync','html:watch','sass:watch','images:watch','jsPages:watch']);

//build
gulp.task('build', ['html','sass','images','jsPages','templatejs']);